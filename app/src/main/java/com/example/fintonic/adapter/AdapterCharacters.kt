package com.example.fintonic.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.fintonic.R
import com.example.fintonic.model.Model
import com.google.android.material.card.MaterialCardView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_character.*

abstract class AdapterCharacters(
    val context: Context,
    var arrayList: ArrayList<Model.MarvelCharacter>
) :
    RecyclerView.Adapter<AdapterCharacters.AdapterCharactersViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterCharactersViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return AdapterCharactersViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int = arrayList.size

    override fun onBindViewHolder(holder: AdapterCharactersViewHolder, position: Int) {
        val card: Model.MarvelCharacter = arrayList[position]
        holder.bind(card)
    }

    inner class AdapterCharactersViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.row_character, parent, false)) {

        private var imgCharacter: ImageView? = null
        private var textViewName: TextView? = null
        private var textViewAlias: TextView? = null
        private var cardView: MaterialCardView? = null
        private var linearLayout: LinearLayout? = null

        init {
            imgCharacter = itemView.findViewById(R.id.imgCharacter)
            textViewAlias = itemView.findViewById(R.id.textViewAlias)
            textViewName = itemView.findViewById(R.id.textViewName)
            cardView = itemView.findViewById(R.id.cardView)
            linearLayout = itemView.findViewById(R.id.linearLayout)
        }

        fun bind(character: Model.MarvelCharacter) {
                linearLayout!!.visibility = View.VISIBLE

                Picasso.with(context).load(character.photo).into(imgCharacter)

                textViewName!!.text = character.name
                textViewAlias!!.text = character.realName

                cardView!!.setOnClickListener {
                    getCharacter(character)
                }
        }
    }

    fun refreshData(arrayListData: ArrayList<Model.MarvelCharacter>) {
        arrayList = arrayListData
        notifyDataSetChanged()
    }

    abstract fun getCharacter(character: Model.MarvelCharacter)
}