package com.example.fintonic.model

import java.io.Serializable

object Model {
    data class MarvelCharacter(
        var name: String,
        var photo: String,
        var realName: String,
        var height: String,
        var power: String,
        var abilities: String,
        var groups: String
    ): Serializable

    data class Superheroes(var superheroes: ArrayList<MarvelCharacter>)
}