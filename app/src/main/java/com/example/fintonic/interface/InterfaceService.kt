package com.example.fintonic.`interface`

import com.example.fintonic.model.Model
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface InterfaceService {

    @GET("bvyob")
    fun getAll(@Query("superheroes") action: String): Observable<Model.Superheroes>

    companion object {
        fun create(): InterfaceService {

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://api.myjson.com/bins/")
                .build()

            return retrofit.create(InterfaceService::class.java)
        }
    }
}