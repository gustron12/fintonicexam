package com.example.fintonic.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.fintonic.R
import com.example.fintonic.`interface`.InterfaceService
import com.example.fintonic.adapter.AdapterCharacters
import com.example.fintonic.model.Model
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var disposable: Disposable? = null
    private var marvelCharacters: ArrayList<Model.MarvelCharacter> = ArrayList()

    private val wikiApiServe by lazy {
        InterfaceService.create()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapterCharacters: AdapterCharacters =
            object : AdapterCharacters(this@MainActivity, marvelCharacters) {
                override fun getCharacter(character: Model.MarvelCharacter) {
                    val intent = Intent(this@MainActivity, CharacterDetail::class.java)
                    intent.putExtra("character", character)
                    startActivity(intent)
                }
            }

        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = adapterCharacters
        }

        recyclerView.showShimmer()

        disposable = wikiApiServe.getAll("")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    recyclerView.hideShimmer()
                    marvelCharacters.addAll(result.superheroes)
                    adapterCharacters.refreshData(marvelCharacters)
                },
                { error ->
                    Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show()
                }
            )

        textInputLayoutSearch.editText!!.addTextChangedListener(
            object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {

                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                    if (s.toString().isNotEmpty()) {
                        disposable = Observable.fromIterable(marvelCharacters)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .filter { character ->
                                character.name.toLowerCase().contains(s.toString()) || character.realName.toLowerCase().contains(
                                    s.toString()
                                )
                            }
                            .toList()
                            .subscribeWith(object :
                                DisposableSingleObserver<List<Model.MarvelCharacter>>() {
                                override fun onSuccess(value: List<Model.MarvelCharacter>?) {
                                    if (value?.size!! > 0) {
                                        textViewNoResults!!.visibility = View.GONE
                                        recyclerView!!.visibility = View.VISIBLE

                                        adapterCharacters.refreshData(value as ArrayList<Model.MarvelCharacter>)
                                    } else {
                                        textViewNoResults!!.visibility = View.VISIBLE
                                        recyclerView!!.visibility = View.GONE

                                        textViewNoResults!!.text = "No results for '${s.toString()}'"
                                    }
                                }

                                override fun onError(e: Throwable?) {
                                    Log.e("ERROR", "ERROR ${e?.message}")
                                }
                            })
                    } else {
                        adapterCharacters.refreshData(marvelCharacters)
                    }
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }
            }
        )
    }
}
