package com.example.fintonic.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.fintonic.R
import com.example.fintonic.model.Model
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_character_detail.*

class CharacterDetail : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_character_detail)

        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        toolbar?.navigationIcon =
            ContextCompat.getDrawable(this, R.drawable.ic_keyboard_arrow_left_black_24dp)
        toolbar?.setNavigationOnClickListener { finish() }

        var character: Model.MarvelCharacter =
            intent.extras!!.getSerializable("character") as Model.MarvelCharacter

        Picasso.with(this@CharacterDetail).load(character.photo).into(imgCharacter)

        textViewName!!.text = character.name
        textViewAlias!!.text = character.realName

        textViewAbilities!!.text = character.abilities
        textViewGroups!!.text = character.groups
        textViewHeight!!.text = character.height
        textViewPower!!.text = character.power
    }
}